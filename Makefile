#
# Copyright (c) 2017 HlepOS Ltd.
#
KERNEL_SOURCE_VERSION ?= $(shell uname -r)
KERNEL_TREE ?= /lib/modules/$(KERNEL_SOURCE_VERSION)/build

EXTRA_CFLAGS += -I$(KERNEL_TREE)/drivers/md -I./ -DCOMMIT_REV="\"$(COMMIT_REV)\""
EXTRA_CFLAGS += -I$(KERNEL_TREE)/include/ -I$(KERNEL_TREE)/include/linux 

obj-m += hlepaoe.o
hlepaoe-objs := aoeblk.o aoechr.o aoecmd.o aoedev.o aoemain.o aoenet.o

.PHONY: all
all: modules

.PHONY:    modules
modules: $(RHEL5_SETUP)
	make -C $(KERNEL_TREE) M=$(PWD) modules V=0

.PHONY: modules_install
modules_install: modules
	install -o root -g root -m 0755 -d $(DESTDIR)/lib/modules/$(KERNEL_SOURCE_VERSION)/kernel/drivers/block/
	install -o root -g root -m 0755 hlepaoe.ko $(DESTDIR)/lib/modules/$(KERNEL_SOURCE_VERSION)/kernel/drivers/block/
	depmod -a $(KERNEL_SOURCE_VERSION)

.PHONY: install
install: modules_install

.PHONY: clean
clean:
	make -C $(KERNEL_TREE) M=$(PWD) clean
