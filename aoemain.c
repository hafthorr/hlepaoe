/* Copyright (c) 2013 Coraid, Inc. 
 * Copyright (c) 2017 HlepOS Ltd.
 *
 * aoemain.c
 * Module initialization routines, discover timer
 */

#include <linux/hdreg.h>
#include <linux/blkdev.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include "hlepaoe.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("HlepOS Ltd");
MODULE_DESCRIPTION("HlepOS AoEv2");
MODULE_VERSION(VERSION);

static struct timer_list timer;

static void discover_timer(struct timer_list *t)
{
	mod_timer(t, jiffies + HZ * 60); /* one minute */

	aoecmd_cfg(0xffff, 0xff);
}

static void
aoe_exit(void)
{
	del_timer_sync(&timer);

	aoenet_exit();
	unregister_blkdev(AOE_MAJOR, DEVICE_NAME);
	aoecmd_exit();
	aoechr_exit();
	aoedev_exit();
	aoeblk_exit();		/* free cache after de-allocating bufs */
}

static int __init
aoe_init(void)
{
	int ret;

	ret = aoedev_init();
	if (ret)
		return ret;
	ret = aoechr_init();
	if (ret)
		goto chr_fail;
	ret = aoeblk_init();
	if (ret)
		goto blk_fail;
	ret = aoenet_init();
	if (ret)
		goto net_fail;
	ret = aoecmd_init();
	if (ret)
		goto cmd_fail;
	ret = register_blkdev(AOE_MAJOR, DEVICE_NAME);
	if (ret < 0) {
		printk(KERN_ERR "hlepaoe: can't register major\n");
		goto blkreg_fail;
	}
	printk(KERN_INFO "hlepaoe: AoE v%s initialised.\n", VERSION);

	timer_setup(&timer, discover_timer, 0);
	discover_timer(&timer);
	return 0;
 blkreg_fail:
	aoecmd_exit();
 cmd_fail:
	aoenet_exit();
 net_fail:
	aoeblk_exit();
 blk_fail:
	aoechr_exit();
 chr_fail:
	aoedev_exit();

	printk(KERN_INFO "hlepaoe: initialisation failure.\n");
	return ret;
}

module_init(aoe_init);
module_exit(aoe_exit);

