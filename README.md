# hlepaoe

AoE Initiator with fixes and 4k support
fork of official kernel module

apt update

apt install dkms libelf-dev

cd /usr/src

git clone git@bitbucket.org:hafthorr/hlepaoe.git

mv hlepaoe hlepaoe-1.0.0

dkms add -m hlepaoe -v 1.0.0

dkms install hlepaoe/1.0.0

modprobe hlepaoe

modinfo hlepaoe

lsmod |grep hlepaoe
